// kartice koje prikazuju razlicite clanke
const tabs = document.querySelectorAll('.tab-btn');
const all_content = document.querySelectorAll('.tab-content');

// postavljanje default kartice
tabs[0].classList.add('active'); 
all_content[0].classList.add('active');

// prolazak po svim buttons i postavljanje onClick funkcije
tabs.forEach((tab, index) =>{
    tab.addEventListener('click', (e)=>{
        // micanje "active" sa svih tabs
        tabs.forEach(tab=>{tab.classList.remove('active')});
        // postavljanje "active" na pravi tab
        tab.classList.add('active');

        // konfiguracija linije koja oznacava active tab
        var line = document.querySelector('.line');
        line.style.width = e.target.offsetWidth + "px";
        line.style.left = e.target.offsetLeft + "px";

        // micanje "active" sa svih content
        all_content.forEach(content => {content.classList.remove('active')});
        // postavljanje "active" na pravi content
        all_content[index].classList.add('active');
    })
})
// responzivni navbar
function Menu(e){
	let list = document.getElementById("navbar");
  
	// izmjenjivanje ikona hamburger menu-a
	e.name === 'menu' ? (e.name = "close", 
	list.classList.toggle('active')) : (e.name = "menu", 
	list.classList.remove('active'))
}

// brojcani prikaz kolicine proizvoda u kosarici
function refreshCartItems(){
	let numberOfItems = 0;
	let cartNumber = document.querySelector("#cart-items");
	let mapValueString = localStorage.getItem("cart");
	let map = new Map(JSON.parse(mapValueString));
	
	// zbrajanje kolicine proizvoda u konacnu kolicinu
	for (let value of map.values()) {
		numberOfItems += value;
	}

	cartNumber.textContent = numberOfItems;
}

refreshCartItems();
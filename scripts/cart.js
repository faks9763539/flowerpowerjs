// uzimanje podataka iz localStorage-a i pohrana u mapu
function getCart() {
    let mapString = localStorage.getItem("cart");
    let map = new Map(JSON.parse(mapString));
    return map;
}

// uzimanje promo code-a koji je korisnik upisao 
// i spremanje podatka o stanju promo code-a
function changeCodeState() {
    // uzimanje stanja promo code-a s localStorage-a
    let codeValueString = localStorage.getItem("code");
    let codeValue = JSON.parse(codeValueString);
    // uzimanje code-a upisanog u input field
    let value = document.querySelector('#promo-code-text').value;

    // provjera upisanog code-a
    if (value == "KSLUNA01"){
        codeValue = true;
    } else {
        codeValue = false;
    }

    // postavljanje stanja promo code-a u localStorage
    localStorage.setItem("code", codeValue);
    refreshCart();
}

// micanje proizvoda iz kosarice
function removeFromCart(id) {
    let map = getCart();

    // uzimanje kolicine proizvoda u kosarici i umanjivanje za 1
    // ako broj padne na 0 - micanje iz mape
    let counter = map.get(id);  
    if (counter > 1) {
        counter--;
        map.set(id, counter);
    } else {
        map.delete(id);
    }

    // dizanje cart-a nazad na localStorage
    mapString = JSON.stringify([...map]);
    localStorage.setItem("cart", mapString);

    refreshCart();
    refreshCartItems();
}

// ispisivanje tablice s odabranim proizvodima, 
// provjera promo code-a i primjena (po potrebi)
// ispis promo code statusa
let refreshCart = async function () {
    let map = getCart();
    if(map){
        // postavljanje elemenata koji su na stranici 
        // neovisno o kolicini proizvoda u cart-u
        let container = document.querySelector('.cart');
		container.innerHTML = "";
        
        let cartHeaderTemplate = document.querySelector('#cart-template-header');
        let cartHeader = cartHeaderTemplate.content.cloneNode(true);
        container.appendChild(cartHeader);
        
        if (!localStorage.getItem("code")){
            let codeValue = false;
            localStorage.setItem("code", codeValue);
        }

        let submitButton = document.querySelector('#promo-code-button');
        submitButton.onclick = () => {changeCodeState()}
        
        let codeValueString = localStorage.getItem("code")
        let codeValue = JSON.parse(codeValueString);
		let response = await fetch("data/flowerShop.json")
        
        let promoCodeText = document.querySelector('#promo-code-status');
        if (codeValue){
            promoCodeText.textContent = "20% Discount applied. Enjoy!";
        } else {
            promoCodeText.textContent = "Discount not applied.";
        }
        
        // ako je cart prazan zavrsava postavljanje elemenata na stranici
        if(map.size < 1) return;

        // kreiranje tablice s proizvodima u cart-u
        let data = await response.json();
        
        let cartItemTemplate = document.querySelector('#cart-template-item');
        for(const id of map.keys()){
            let product = data.products.find(p => p.id == id);
            let cartItem = cartItemTemplate.content.cloneNode(true);
            
            cartItem.querySelector(".cart-item").dataset.id = id;
            let title = cartItem.querySelector('.cart-item-title');
            title.textContent = product.name;
            let quantity = cartItem.querySelector('.cart-item-quantity');
            quantity.value = map.get(id);
                
            let price = cartItem.querySelector('.cart-item-price');
            if (codeValue){
                price.textContent = String(Number(product.price) * 0.8) + " €";
            } else {
                price.textContent = product.price + " €";
            }

            let removeButtonElement = cartItem.querySelector('.remove-button');
            removeButtonElement.onclick = () => {
                removeFromCart(id);
            }

            container.appendChild(cartItem);
        }
    }
}

refreshCart();
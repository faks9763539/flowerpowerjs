// dodavanje proizvoda u cart
function addToCart(id) {
	// ako nista nije dodano u cart 
	// kreiranje mape s kljucem cart u localStorage-u
	if (!localStorage.getItem("cart")){		
		let newMap = new Map([[id, 1]]);
		let newMapString = JSON.stringify([...newMap]);
		localStorage.setItem("cart", newMapString);
	} else {
		// ako proizvod vec popstoji u cart-u dodavanje 1
		// ako proizvod ne postoji, dodavanje proizvoda u cart
		let mapString = localStorage.getItem("cart");
		let map = new Map(JSON.parse(mapString));
		if(!map.has(id)){
			map.set(id, 1);
		} else {
			let counter = map.get(id);
			counter++;
			map.set(id, counter);
		}
		// postavljanje cart-a u localStorage
		mapString = JSON.stringify([...map]);
		localStorage.setItem("cart", mapString);
	}
	refreshCartItems();
}

// dohvacanje podataka iz json-a
let getData = async function () {
	let response = await fetch("data/flowerShop.json");
	let data = await response.json();
	addCategories(data);
}

// pomoćna funkcija funkciji "getData"
// vizualno uredivanje stranice, ispis svih kategorija
// i proizvoda koji se nalaze u njima
let addCategories = async function (data) {
	let categories = data.categories;
	let main = document.querySelector('main');
	let categoryTemplate = document.querySelector('#category-template');

	// kreiranje strukture stranice - kategorije
	for (let index = 0; index < categories.length; index++) {
		let category = categoryTemplate.content.cloneNode(true);
		let categoryTitleElement = category.querySelector('.decorated-title > span');
		categoryTitleElement.textContent = categories[index].name;
		
		let products = data.products.filter(p => p.categoryId == categories[index].id);

		let productTemplate = document.querySelector('#product-template');
		let gallery = category.querySelector('.gallery');

		// u svaku kategoriju dodaju se proizvodi
		for (let productIndex = 0; productIndex < products.length; productIndex++) {
			let product = productTemplate.content.cloneNode(true);

			let productPhotoBox = product.querySelector('.photo-box');
			productPhotoBox.setAttribute("data-id", products[productIndex].id);

			let productImageElement = product.querySelector('.photo-box-image');
			productImageElement.src = products[productIndex].imageUrl;

			let productTitleElement = product.querySelector('.photo-box-title');
			productTitleElement.textContent = products[productIndex].name;

			let productButtonElement = product.querySelector('.cart-btn');
			productButtonElement.onclick = () => {
				addToCart(products[productIndex].id);
			};

			gallery.appendChild(product);
		}

		main.appendChild(category);
	}
};
getData();
// slideshow
(function() {
  // trazenje slika koje se trebaju izmjenjivati 
  var images = document.querySelectorAll('.scaled-image');
  var currentIndex = 0;

  // prikaz odredene slike
  function showImage(index) {
    for (var i = 0; i < images.length; i++) {
      if (i === index) {
        images[i].style.display = 'block';
      } else {
        images[i].style.display = 'none';
      }
    }
  }

  // prikaz iduce slike na redu
  function nextImage() {
    currentIndex++;
    if (currentIndex >= images.length) {
      currentIndex = 0;
    }
    showImage(currentIndex);
  }

  // promjena slike svakih 2 sekunde (2000 milisekundi)
  function startSlideshow() {
    setInterval(nextImage, 2000); 
  }

  showImage(currentIndex);
  startSlideshow();
})();